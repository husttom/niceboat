<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="/WEB-INF/tlds/cms.tld" %>
<%@ taglib prefix="shiro" uri="/WEB-INF/tlds/shiros.tld" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="ligerUIUrl" value="${pageContext.request.contextPath}/ui/plugins/ligerUI"/>
<c:set var="plugins" value="${pageContext.request.contextPath}/ui/plugins"/>
<c:set var="img_path" value="${pageContext.request.contextPath}/static/images"/>
<c:set var="flash_path" value="${pageContext.request.contextPath}/static/flash"/>
