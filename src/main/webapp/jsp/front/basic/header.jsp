<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>
	<c:if test="${cms:getDefaultSite()!=null}">${cms:getDefaultSite().title}</c:if>
	</title>
	<meta name="keywords" content="<c:if test="${cms:getDefaultSite()!=null}">${cms:getDefaultSite().keywords}</c:if>">
	<meta name="description" content="<c:if test="${cms:getDefaultSite()!=null}">${cms:getDefaultSite().description}</c:if>">
    <link href="${ctx}/static/css/reset_panda.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/static/css/style.css" rel="stylesheet" type="text/css" />
    <link href="${ctx}/static/coin-slider/coin-slider-styles.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${ctx}/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/jquery.sgallery.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript" src="${ctx}/static/js/global.js"></script>
	<script type="text/javascript" src="${ctx}/static/coin-slider/coin-slider.js"></script>
	<!--[if IE 6]>
	<script src="DD_belatedPNG.js"></script>
	<script>
	  DD_belatedPNG.fix('.nav,.tips,.about_left h1');
	</script>
	<![endif]-->
	<style type="text/css">
	
/* 图片轮播css */
/* scrollBox_a1 */
.scrollBox_a1 {
	float: left;
	width: 980px;
	height: 250px;
	padding: 2px;
	position: relative;
	/*border: 1px solid #aaa;*/
}

.scrollBox_a1 .a_bigImg img {
	position: absolute;
	top: 2px;
	left: 2px;
	display: none;
}

/* ul_scroll_a2 */
.ul_scroll_a2 {
	position: absolute;
	right: 5px;
	bottom: 7px;
	padding-left: 19px;
	overflow: hidden;
}

.ul_scroll_a2 li {
	display: -moz-inline-stack;
	display: inline-block;
	*display: inline;
	*zoom: 1;
}

.ul_scroll_a2 span {
	display: -moz-inline-stack;
	display: inline-block;
	*display: inline;
	*zoom: 1;
	font-size: 0.8em;
	padding: 0px 3px;
	margin-right: 2px;
	border: 1px solid #999;
	background: #fff;
	filter: alpha(opacity = 85);
	opacity: 0.85;
	cursor: hand;
	cursor: pointer;
}

.ul_scroll_a2 span.on {
	border: 1px solid #CC0000;
	background: #FFFF9D;
	color: #CC0000;
}
</style>
</head>
<body>
<div class="web center">
	<div class="tips center">
		<div class="notice left">
			<div id="announ">
			<ul>
			<c:forEach items="${cms:getArticles(11,1)}" var="it" varStatus="status" >
				<li><a href="${ctx}/aa/${it.id}.html" class="bright" target="_blank">${it.title}</a></li>
			</c:forEach>
			</ul>
			</div>
			<script type="text/javascript">
				$(function(){
					startmarquee('announ',40,1,500,4000);
				})
			</script>
		</div>
			<div class="setup right">
			<ul>
				<li><a href="#" id="sethomepage">设为首页</a>|<a href="#" id="favorites">加入收藏</a></li>
			</ul>
		</div>
	</div>
	<div class="header center">
		<a href="${ctx}/index"><img src="${img_path}/logo.jpg" /></a>
		<ul class="nav">
		<li class="left"><a href="${ctx}/index">首 页<br><span>HOME</span></a></li>
		<li class="left"><a href="${ctx}/aboutUs">了解NB CMS<br><span>ABOUT US</span></a></li>
		<li class="left"><a href="${ctx}/manual">用户手册<br><span>MANUAL</span></a></li>
		<li class="left"><a href="${ctx}/product01">页面演示<br><span>PRODUCT</span></a></li>
		<li class="left"><a href="${ctx}/tutorial">教程<br><span>TUTORIAL</span></a></li>
		<li class="left"><a href="${ctx}/join">加入我们<br><span>JOIN</span></a></li>
		<li class="left"><a href="${ctx}/contact">联系我们<br><span>CONTACT</span></a></li>
		<li class="left"><a href="${ctx}/message">留言板<br><span>MESSAGE</span></a></li>
		</ul>
	</div>