<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<script>

$(function(){
	if ("${param.pid}"!=""){
		$("#pid").val("${param.pid}");
	}
	Free.validateSubmitAndClose($("#inputForm"));
	
});
</script>

<style>
.content input{
width:300px;
}
.content p{
width:500px;
}
</style>
<form id="inputForm" class="form-horizontal"  action="${ctx}/system/menu/save.do">

<input type="hidden" name="id" value="${ob.id}" />

	<div class="control-group">
		<label class="control-label" for="">菜单名称</label>
		<div class="controls">
			<input name="menuname" class="span6" value="${ob.menuname}" type="text" validate="{required:true}"  maxlength="50" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">菜单url</label>
		<div class="controls">
			<input name="url" class="span6" value="${ob.url}" type="text" validate="{required:true}"  maxlength="100" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">父级菜单ID</label>
		<div class="controls">
			<input name="pid" class="span6" value="${ob.pid}" type="text" validate="{required:true}"  maxlength="20" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">是否显示</label>
		<div class="controls">
			<input name="sfxs" class="span6" value="${ob.sfxs}" type="text" validate="{required:true}"  maxlength="2" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="">顺序</label>
		<div class="controls">
			<input name="sx" class="span6"  value="${ob.sx}" type="text" validate="{required:true}"  maxlength="2" />
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<button class="btn btn-success" id="free_submit" type="button"> 保存 </button>
			<button class="btn" type="reset"> 重置 </button>
		</div>
	</div>


</form>
