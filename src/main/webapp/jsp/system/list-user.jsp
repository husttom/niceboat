<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>

<script type="text/javascript">
$(function (){
	ligergrid =  $("#maingrid").ligerGrid({
            columns: 
				[ 
				 {display : 'ID',name : 'id',width :"5%"}, 
				 {display : '登录名',name : 'loginname'}, 
				 {display : '用户名',name : 'xm'}, 
				 {display : 'Email',name : 'mail',width :"20%"}, 
				 {display : '手机',name : 'phone'},
				 {
                     display: '操作', isAllowHide: false,
                     render: function (row)
                     {
                         var html ="";

							<shiro:hasPermission name="user:edit">
                        html+='<button class="btn btn-mini btn-info" onclick="f_edit({0})" type="button">编辑</button>&nbsp;';
                        </shiro:hasPermission >
						<shiro:hasPermission name="user:delete">
                        html+='<button class="btn btn-mini btn-danger" onclick="f_delete({0})" type="button">删除</button>';
							</shiro:hasPermission >
                        return Free.replace(html,row.id);
                     }, width: 160
                 }

            ],
            url: '${ctx}/system/user/ajax_list.do',
            pageSize: 20, sortName: 'id',
            width: '99%', height: '98%', 
            checkbox : false,
            pageParmName:'page'
        });

    });
function f_query(){
	ligergrid.set('parms',$('#query-form').serializeArray());
	ligergrid.loadData();
}
function f_edit(id){
	parent.newDialog({type:'iframe',value:'${ctx}/system/user/input.do?id='+id},
				{modal:true,width:450,height:350});
}

function f_add(){
	parent.newDialog({type:'iframe',value:'${ctx}/jsp/system/input-user.jsp'},
		{modal:true,width:450,height:350});
}
function f_delete(id){
	  freeConfirm("是否将此信息删除?",function(){
	  Free.ajax({
       	   url: '${ctx}/system/user/delete.do',
       	   data: {id:id},
       	   success: function(data){
       		   if (data='ok'){
       			 ligergrid.loadData();
       		   }
        	   }
	  });
	});
}
</script>
</head>
<body>
        <table class="tab">
          <tbody>
          <tr class="tab_white02">
            <td>     
			<form class="span12 form-horizontal" id="query-form" method="get" action="stat-system">
						登录名：
						<input type="text" class="span2" placeholder="精确查询..." name="search_EQ_loginname">
						用户名：
						<input type="text" class="span2" placeholder="模糊查询..." name="search_LIKE_xm">
					
							<input type="button" onclick="f_query();" value="查询" class="btn btn-danger">
							<button class="btn" type="reset">重置</button>
							<shiro:hasPermission name="user:add">
								<input type="button" onclick="f_add();" value="新增" class="btn btn-success">
							</shiro:hasPermission> 
					
			</form>
            </td>
          </tr>
        </tbody></table>
     
        <div id="maingrid" ></div>
    
</body>	

</html>
