package com.nb.system.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.free.common.utils.web.Servlets;
import com.nb.common.utils.SearchFilter;
import com.nb.system.NewPager;
import com.nb.system.model.Authorize;
import com.nb.system.service.AuthorizeService;

/**
 * 
 * 模板自动生成   for FreeUI
 * @author mefly
 *
 */
@Controller
@RequestMapping(value="/admin")
public class AdminController {
	
	/*
	 * 
	 */
	@RequestMapping("/index")
	public String index(HttpServletRequest request){
		return "index";
	}
	
}