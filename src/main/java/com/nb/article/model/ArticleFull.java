﻿package com.nb.article.model;

/**
 * 会议文件附件
 * 模板自动生成   for FreeUI
 * @author mefly
 *
 */
public class ArticleFull extends Article{

    private String content;
    
    //-----------------------
    
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}