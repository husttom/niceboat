﻿package com.nb.article.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nutz.dao.Condition;
import org.nutz.dao.Dao;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.article.model.Article;
import com.nb.common.service.BaseService;
import com.nb.system.NewPager;


/**
 * 文章管理实现类
 * @author 赵占涛 369880281@qq.com
 *
 */
@Service("articleService")
public class ArticleService extends BaseService{

	@Autowired
	private Dao dao;
	


	public List<Article> getAll(Condition cnd) {
		return dao.query(Article.class, cnd);
	}
	
	public Map<String,Object> queryPage(NewPager page){
		page.setOrderBy("sx");
		page.setOrder("desc");
		Criteria cri = getCriteriaFromPage(page);
		
	    List<Article> list = dao.query(Article.class, cri, page);
	    page.setRecordCount(dao.count(Article.class, cri));
	    
	    Map<String,Object> map = new HashMap<String,Object>();
		map.put("Total", page.getRecordCount());
		map.put("Rows", list);
	    return map;
	}
	
	public Article insert(Article record) {
		return dao.insert(record);
	}

	public Article fetch(Integer id) {
		return dao.fetch(Article.class,id);
	}
	
	public int updateIgnoreNull(Article record) {
		return dao.updateIgnoreNull(record);
	}
	public int delete(Integer id) {
		return dao.delete(Article.class,id);
	}
}
