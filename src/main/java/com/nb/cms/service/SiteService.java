﻿package com.nb.cms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.cms.model.Site;
import com.nb.common.service.BaseService;
import com.nb.common.utils.CacheUtils;
import com.nb.system.NewPager;


/**
 * 站点管理
 * @author mefly
 *
 */
@Service
public class SiteService  extends BaseService{

	private static final String CMS_CACHE = "cmsCache";
	
	@Autowired
	private Dao dao;

	public List<Site> getAll() {
		return dao.query(Site.class, null);
	}

	public int delete(int id) {
		return dao.delete(Site.class, id);
	}

	public Site fetch(int id) {
		return dao.fetch(Site.class, id);
	}

	public Site fetchDefaultSite() {
		Site site = (Site)CacheUtils.get(CMS_CACHE, "defaultSite");
		if (site == null){
			site = dao.fetch(Site.class, Cnd.where("isdefault","=","1"));
			CacheUtils.put(CMS_CACHE, "defaultSite", site);
		}
    	return site;
	}

	public Map<String, Object> queryPage(NewPager page) {
		page.setOrderBy("id");
		page.setOrder("desc");
		Criteria cri = getCriteriaFromPage(page); 
		
	    List<Site> list = dao.query(Site.class, cri, page);
	    page.setRecordCount(dao.count(Site.class, cri));
	    
	    Map<String,Object> map = new HashMap<String,Object>();
		map.put("Total", page.getRecordCount());
		map.put("Rows", list);
	    return map;
	}

	public Site insert(Site en) {
		return dao.insert(en);
	}

	public void updateIgnoreNull(Site en) {
		dao.updateIgnoreNull(en);
	}
	
}
