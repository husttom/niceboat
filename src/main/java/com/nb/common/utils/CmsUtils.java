
package com.nb.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.nutz.dao.Cnd;
import org.nutz.dao.Condition;
import org.nutz.dao.Dao;
import org.nutz.dao.pager.Pager;

import com.google.gson.Gson;
import com.nb.article.model.Article;
import com.nb.article.model.ArticleData;
import com.nb.article.model.ArticleFull;
import com.nb.category.model.Category;
import com.nb.cms.model.Site;
import com.nb.cms.service.SiteService;

/**
 * 内容管理工具类
 */
public class CmsUtils {

	private static final String CMS_CACHE = "cmsCache";
	
	private static SiteService siteService = SpringContextHolder.getBean(SiteService.class);
	//private static CategoryService categoryService = SpringContextHolder.getBean(CategoryService.class);
	private static Dao dao = SpringContextHolder.getBean(Dao.class);

    /**
     * 得到文章
   	 */
    public static List<Article> getArticles(Integer categoryid,Integer num) {
    	Condition c = Cnd.where("categoryid","=",categoryid);
    	Pager pager = dao.createPager(1, num);
        List<Article> list = dao.query(Article.class, c, pager);
    	return list;
    }
    
    /**
     * 得到文章
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
   	 */
    public static ArticleFull getArticle(Long articleid) throws IllegalAccessException, InvocationTargetException {
    	ArticleFull articleFull = new ArticleFull();
    	BeanUtils.copyProperties(articleFull, dao.fetch(Article.class, articleid));
    	ArticleData articleData = dao.fetch(ArticleData.class,Cnd.where("pid","=",articleid));
    	articleFull.setContent(articleData.getContent());
    	return articleFull;
    }    
    /**
     * 得到文章内容
   	 */
    public static ArticleData getArticleData(Long articleid) {
    	return dao.fetch(ArticleData.class,Cnd.where("pid","=",articleid));
    } 
    /**
     * 得到当前站点
   	 */
    public static Site getDefaultSite() {
    	return siteService.fetchDefaultSite();
    }    

    
    public static List<Category> getCategory(Long pid) {
    	return dao.query(Category.class, Cnd.where("pid", "=", pid));
    }
    
	public static void removeCache(String key) {
		CacheUtils.remove(CMS_CACHE, key);
	}
}